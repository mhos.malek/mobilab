const dotenv = require('dotenv');
const pathTo = require('./pathTo');
const env = dotenv.config({ path: pathTo.mainEnvSrc }).parsed;

const getEnvVariables = () =>
  Object.keys(env).reduce((res, current) => {
    res[`process.env.${current}`] = JSON.stringify(env[current]);
    return res;
  }, {});

exports.getEnvVariables = getEnvVariables;
