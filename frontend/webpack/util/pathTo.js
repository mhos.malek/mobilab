const path = require('path');

const projectRoot = path.resolve(__dirname, '../..');

/**
 * Paths to directories and files of the project.
 */
module.exports = {
  projectRoot,

  // Files
  entryPointSrc: path.resolve(projectRoot, 'src/app.js'),
  htmlTemplateSrc: path.resolve(projectRoot, 'src/template.html'),
  favIconSrc: path.resolve(
    projectRoot,
    'src/assets/images/cropped-fav-icon-512-192x192.png',
  ),
  reportSrc: path.resolve(projectRoot, 'analyses/logs/logs.html'),
  mainEnvSrc: path.resolve(projectRoot, '.env.sample'),

  // Directories
  srcDir: path.resolve(projectRoot, 'src'),
  distDir: path.resolve(projectRoot, 'dist'),
  componentsDir: path.resolve(projectRoot, 'src/components'),
  storeDir: path.resolve(projectRoot, 'src/store'),
  servicesDir: path.resolve(projectRoot, 'src/services'),
  utilsDir: path.resolve(projectRoot, 'src/utils'),
  nodeModulesDir: path.resolve(projectRoot, 'node_modules'),
};
