// webpack core packages
const webpack = require('webpack');
const merge = require('webpack-merge');

// optimization plugins and tools
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');

// webpack utils
const common = require('./webpack.common');
const webpackPlugins = require('./plugins');

module.exports = merge(common, {
  mode: 'production',
  devtool: 'source-map',
  optimization: {
    minimizer: [
      new OptimizeCSSAssetsPlugin(),
      new webpack.optimize.OccurrenceOrderPlugin(),
    ],
    splitChunks: {
      chunks: 'all',
    },
  },
  plugins: webpackPlugins.prodPlugins,
});
