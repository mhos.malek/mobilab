const commonPlugins = require('./commonPlugins');
const devPlugins = require('./devPlugins');
const prodPlugins = require('./prodPlugins');

module.exports = {
  commonPlugins,
  devPlugins,
  prodPlugins,
};
