// webpack plugins
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

// utils
const devMode = process.env.NODE_ENV === 'production' ? false : true;
const pathTo = require('../../util/pathTo');

module.exports = [
  new MiniCssExtractPlugin({
    filename: devMode ? '[name].css' : '[name].[hash].css',
    chunkFilename: devMode ? '[id].css' : '[id].[hash].css',
  }),
  new HtmlWebpackPlugin({
    title: 'Mobilab  Code Challenge',
    filename: 'index.html',
    languageCode: 'en-EN',
    template: pathTo.htmlTemplateSrc,
    inject: true,
    favicon: pathTo.favIconSrc,
  }),
];
