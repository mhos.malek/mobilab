/* prod plugins */

const webpack = require('webpack');
const WebpackBundleAnalyzerPlugin = require('webpack-bundle-analyzer')
  .BundleAnalyzerPlugin;
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

// utils
const pathTo = require('../../util/pathTo');

module.exports = [
  new CleanWebpackPlugin(),
  new WebpackBundleAnalyzerPlugin({
    analyzerMode: 'static',
    reportFilename: pathTo.reportSrc,
    defaultSizes: 'gzip',
  }),
];
