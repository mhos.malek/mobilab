/// <reference types="cypress" />

context('test API calls', () => {
  beforeEach(() => {
    const clientApplicationUrl = Cypress.env('CLIENT_APPLICATION_URL');
    const nodeBaseUrl = Cypress.env('NODE_BASE_URL');
    const nodeBaseAPiUrl = `${nodeBaseUrl}/api`;

    // it doesnt accepte defined base url so I had to put it in here;
    cy.visit(clientApplicationUrl);
    cy.wrap(nodeBaseAPiUrl).as('nodeBaseAPIUrl');
  });

  it('should have proper headers and body in API response', function() {
    cy.request(this.nodeBaseAPIUrl).should(response => {
      expect(response.status).to.eq(200);
      expect(response.body.data).to.have.length(60);
      expect(response).to.have.property('headers');
    });
  });

  it('should have proper response with query parameters', function() {
    const defaultQueryParams = {
      showVerify: false,
      sort: 'top',
      window: 'day',
    };
    cy.request({
      url: this.nodeBaseAPIUrl,
      qs: defaultQueryParams,
    }).should(response => {
      const firstItemOfRes = response.body.data[0];
      expect(firstItemOfRes).to.be.an('object');
      expect(firstItemOfRes)
        .property('id')
        .to.be.a('string');
      expect(firstItemOfRes)
        .property('link')
        .to.be.a('string');
    });
  });
});
