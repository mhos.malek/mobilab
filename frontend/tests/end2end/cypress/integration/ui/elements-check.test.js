/// <reference types="cypress" />

context('test UI elements', () => {
  beforeEach(() => {
    cy.visit(Cypress.env('CLIENT_APPLICATION_URL'));
  });

  it('should show the list of element in container', () => {
    cy.xpath('/html/body/div/div[2]/main/main/div')
      .children()
      .should('have.class', 'row');
  });

  it('should show the list of element in row', () => {
    cy.xpath('/html/body/div/div[2]/main/main/div/div[1]')
      .children()
      .should('have.class', 'col-lg-4');
  });

  it('should show the topbar filter and have 4 items as filter', () => {
    cy.xpath('/html/body/div/div[2]/header/div[2]/form')
      .children()
      .should('have.length', 4);
  });
});
