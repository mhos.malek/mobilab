/// <reference types="cypress" />

context('test page changes', () => {
  beforeEach(() => {
    const clientApplicationUrl = Cypress.env('CLIENT_APPLICATION_URL');
    cy.wrap(clientApplicationUrl).as('clientUrl');
    cy.visit(clientApplicationUrl);
  });

  it('should go to 404 page if user enters a wrong address', function() {
    // I hadn't implement any token  for this project but used it for test);
    localStorage.setItem('token', 'sample token for test in client');
    cy.visit(`${this.clientUrl}/not-a-valid-address`);
    cy.location('pathname').should('eq', '/not-a-valid-address');
    cy.contains('not Found');
  });

  it('should have proper seo data after SEO hoc added meta data', function() {
    cy.visit(this.clientUrl);
    cy.title().should('eq', 'Mobilab | Home');
  });
});
