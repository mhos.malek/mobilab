import {combineReducers} from 'redux';
import UiReducer from './ui/reducer';

export default combineReducers({
  Ui: UiReducer,
});
