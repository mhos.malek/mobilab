import { all } from 'redux-saga/effects';
import UiSaga from './ui/sagas';

export default function* sagas() {
  yield all([...UiSaga]);
}
