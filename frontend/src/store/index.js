import {createStore, applyMiddleware} from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';
import sagas from './rootSaga';
import rootReducer from './rootReducer';
const sagaMiddleware = createSagaMiddleware();

// create store with redux saga middleware
const store = createStore(
    rootReducer,
    composeWithDevTools(applyMiddleware(sagaMiddleware))
);

sagaMiddleware.run(sagas);

export default store;
