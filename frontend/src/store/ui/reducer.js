import ActionTypes from './action-types';
import initialState from './initial-state';

const UiReducer = function(state = initialState, action) {
  switch (action.type) {
    case ActionTypes.NOTIFICATION_SHOWN:
      return {
        ...state,
      };
    case ActionTypes.MODAL_SHOWN:
      return {
        ...state,
        showModal: true,
      };
    case ActionTypes.MODAL_HIDDEN:
      return {
        ...state,
        showModal: false,
      };
    case ActionTypes.MODAL_DETAILS_HAS_BEEN_SET:
      return {
        ...state,
        selectedImageDetails: action.payload,
      };
    default:
      return state;
  }
};

export default UiReducer;
