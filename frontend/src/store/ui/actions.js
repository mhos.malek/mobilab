import ActionTypes from './action-types';

export const showNotificationAction = errorPayload => {
  return {
    type: ActionTypes.NOTIFICATION_SHOWN,
    payload: errorPayload,
  };
};

export const showModalOfImageDetailsAction = () => {
  return {
    type: ActionTypes.MODAL_SHOWN,
  };
};

export const hideModalOfImageDetailsAction = () => {
  return {
    type: ActionTypes.MODAL_HIDDEN,
  };
};

export const setModalDetailsFromImageAction = payload => {
  return {
    type: ActionTypes.MODAL_DETAILS_HAS_BEEN_SET,
    payload,
  };
};
