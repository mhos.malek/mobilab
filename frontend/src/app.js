import React from 'react';
import ReactDOM from 'react-dom';
import RootProvider from './providers';

// polyfills

//fetch polyfill
import 'whatwg-fetch';

ReactDOM.render(<RootProvider />, document.getElementById('app'));
