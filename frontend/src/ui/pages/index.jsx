import HomePage from './home-page';
import SamplePage from './sample-page';
import NotFoundPage from './not-found-page';

// use for SEO and React Helmet
const HomePageDetails = {
  component: HomePage,
  meta: {
    pageTitle: 'Mobilab | Home',
    metaContents: [
      {
        name: 'description',
        content: 'Sample Content for MobileLib Home Page',
      },
    ],
  },
};
const SamplePageDetails = {
  component: SamplePage,
  meta: {
    metaContents: [
      {
        name: 'description',
        content: 'Sample Content for MobileLib Sample Page',
      },
    ],
  },
};

const NotFoundPageDetails = {
  component: NotFoundPage,
};

export { HomePageDetails, SamplePageDetails, NotFoundPageDetails };
