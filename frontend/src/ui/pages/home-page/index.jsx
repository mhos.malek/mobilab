// dependecies
import React, { useContext } from 'react';

// HOCs
import withSeo from '@/hoc/seo-hoc';

// smart with logic components
import SelectedItemModal from '@/ui/shared-logic-components/selected-item-modal';

// components
import Filter from '@/ui/pages/home-page/filter';
import filterContext from '../../../providers/filter-provider/filter-context';

const HomePage = () => {
  const { currentFilters } = useContext(filterContext);
  const { section, sort, window, showViral } = currentFilters;
  const filters = {
    section: section.value,
    sort: sort.value,
    window: window.value,
    showViral,
  };
  const createFilterId = () => `imgur-gallery-filter${Math.random()}`;
  return (
    <>
      <Filter filters={filters} filterId={createFilterId()} />
      {<SelectedItemModal />}
    </>
  );
};

export default withSeo(HomePage);
