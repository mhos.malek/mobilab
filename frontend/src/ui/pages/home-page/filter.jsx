// dependecies
import React, { useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import useSWR, { useSWRPages } from 'swr';
import fetcher from '@/services/client';

// custom hooks
import useOnScreen from '@/hooks/on-screen-hook';

// ui components
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';

// base components
import Loading from '@/ui/base-components/loading';

// shared-logic components
import GalleryItems from '@/ui/shared-logic-components/gallery/gallery-items';

// styles
import * as styles from './style.scss';

const Filter = ({ filters }) => {
  // default baseurl of ImgUR API
  const imgUrBaseApiURl = `${process.env.IMGUR_BASE_API_URL}/${process.env.IMGUR_VERSION}`;
  const myProxyBaseApiURl = `${process.env.EXPRESS_PROXY_API_URL}/api?`;
  const { section, sort, window, showViral } = filters;
  const { pages, isLoadingMore, loadMore } = useSWRPages(
    'imgur-gallery-fetch',
    ({ offset, withSWR }) => {
      const url = `${myProxyBaseApiURl}section=${section}&sort=${sort}&window=${window}&page=${offset ||
        0}&showViral=${showViral}`;

      const { data } = withSWR(useSWR(url, fetcher));

      if (!data) return null;
      return data.data.map(imageItem => {
        const {
          images_count,
          images,
          description,
          title,
          ups,
          downs,
          score,
          id,
          is_album,
          is_ad,
          views,
          comment_count,
          link,
          type,
        } = imageItem;

        const dataToShow = {
          description,
          title,
          ups,
          downs,
          score,
          id,
          is_album,
          is_ad,
          views,
          comment_count,
          type,
          link,
          images_count,
          images,
        };
        // TODO: if(is_ad) handle advertise
        // hande albums and videos in this component
        return (
          <Col xs={12} md='auto' lg={4} key={id}>
            <GalleryItems {...dataToShow} />
          </Col>
        );
      });
    },
    (SWR, index) => {
      // there's no next page
      if (SWR.data && SWR.data.length === 0) return null;
      return index + 1;
    },
    [filters],
  );

  const $loadMoreButton = useRef(null);
  const isOnScreen = useOnScreen($loadMoreButton, '200px');

  useEffect(() => {
    if (isOnScreen) loadMore();
  }, [isOnScreen]);

  return (
    <main className={styles.mainPageWrapper}>
      <Container fluid='md'>
        <Row>{pages}</Row>
        {isLoadingMore && <Loading />}
        <div ref={$loadMoreButton} />
      </Container>
    </main>
  );
};

Filter.propTypes = {
  filters: PropTypes.shape({
    section: PropTypes.string,
    sort: PropTypes.string,
    window: PropTypes.string,
    showViral: PropTypes.bool,
  }),
};

export default Filter;
