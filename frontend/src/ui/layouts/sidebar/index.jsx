import React, { useState, useContext } from 'react';
import * as styles from './style.scss';
import Card from 'react-bootstrap/Card';
import Icon from '@/ui/base-components/icon';
import MobilabThemeContext from '@/providers/theme-provider/context';
import classNames from 'classnames';
import FilterImages from '@/ui/shared-logic-components/filter';
import WithOutsideClickHandler from '@/ui/base-components/outsideClick';
const SideBar = () => {
  const [isOpen, setIsOpen] = useState(false);
  const theme = useContext(MobilabThemeContext);

  const defaultRighPaddingAfterOpen = -30;
  const defaultRighPaddingBeforeOpen = -100;

  const fullViewPadding = '-100%'

  const sideBarStyle = {
    right: `${isOpen ? 0 : fullViewPadding}`,
  };
  const buttonStyle = {
    left: `${isOpen ? defaultRighPaddingAfterOpen : -defaultRighPaddingBeforeOpen}px`,
  };
  const getIconColor = () => (isOpen ? theme.colors.primary : null);

  return (
    <WithOutsideClickHandler onOutsideClick={() => setIsOpen(false)}>
      <aside className={styles.wrapper}>
        <Card style={sideBarStyle} className={styles.sideBar}>
          <Card.Body>
            <div className={styles.iconWrapper}>
              <Icon name='adjustments' color={getIconColor()} size='1.5rem' />
            </div>

            <Card.Title>
              <span> Want more controll?</span>
            </Card.Title>
            <FilterImages />
          </Card.Body>
          <button
            onClick={() => setIsOpen(prevState => !prevState)}
            className={classNames(styles.toggleButton, {
              'rotate-animation': isOpen,
            })}
            style={buttonStyle}
          >
            <Icon name='adjustments' color={getIconColor()} size='1rem' />
            <span style={{ color: getIconColor() }}>Filter</span>
          </button>
        </Card>
      </aside>
    </WithOutsideClickHandler>
  );
};

export default SideBar;
