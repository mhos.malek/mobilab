// dependencies
import React, { memo, useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';

// ui components
import classNames from 'classnames';
import Image from 'react-bootstrap/Image';
import { CSSTransition } from 'react-transition-group';

// assets
import logoFile from '../../assets/images/logo-site-svg.svg';
// utils
import { throttle } from '@/utils/general/throttle';
// styles
import * as styles from './style.scss';
import TopbarFilters from './top-filters';
import SideBar from './sidebar';
import useOnScreen from '../../hooks/on-screen-hook';

// eslint-disable-next-line react/display-name
const MainLayout = memo(({ children }) => {
  const [showFixedMenu, setShowFixedMenu] = useState(false);
  const [showSideBar, setShowSideBar] = useState(false);
  const itemScrollHandler = useRef(null);
  const scrollFromTop = useRef(0);
  const handleScroll = function() {
    scrollFromTop.current = window.scrollY;
  };

  const scrollItemStyles = {
    position: 'absolute',
    top: 300,
    right: 0,
  };

  useEffect(() => {
    window.addEventListener('scroll', throttle(handleScroll, 100));
    return function() {
      window.removeEventListener('scroll', handleScroll);
    };
  }, []);

  const hasBeenScrolledMoreThan300px = useOnScreen(itemScrollHandler, '100px');

  useEffect(() => {
    if (hasBeenScrolledMoreThan300px) {
      setShowFixedMenu(false);
    } else {
      const body = window.document.body;
      if (Math.max(body.scrollHeight, body.offsetHeight) > window.innerHeight)
        setShowFixedMenu(true);
      // check if we have enough data to scroll
    }
  }, [hasBeenScrolledMoreThan300px]);

  return (
    <div className={styles.mainLayout}>
      <div style={scrollItemStyles} ref={itemScrollHandler} />
      <CSSTransition
        in={showFixedMenu}
        timeout={{
          enter: 1000,
          exit: 1000,
        }}
        classNames='whole-menu-animation'
      >
        <header
          className={classNames(
            styles.scrollableSapce,
            'whole-menu-animation-exit-done',
          )}
        >
          <div className={styles.fullMenuWrapper}>
            <CSSTransition
              in={showFixedMenu}
              timeout={{
                enter: 1000,
                exit: 1000,
              }}
              classNames='logo-animation'
            >
              <div className={styles.logoWrapper}>
                <Image src={logoFile} alt='logo' fluid />
              </div>
            </CSSTransition>

            <CSSTransition
              in={showFixedMenu}
              timeout={{
                enter: 2000,
                exit: 1000,
              }}
              classNames='fade-text-animation'
            >
              <div className={styles.titleHeader}>
                <span className={styles.titleHeaderBrand}> Imgur </span>
                <span>Gallery</span>
              </div>
            </CSSTransition>
          </div>
          <CSSTransition
            in={!showFixedMenu}
            timeout={{
              enter: 500,
              exit: 500,
            }}
            onEntered={() => setShowSideBar(false)}
            onExited={() => setShowSideBar(true)}
            classNames='fade-text-animation'
          >
            <TopbarFilters />
          </CSSTransition>
        </header>
      </CSSTransition>

      {showSideBar && <SideBar />}

      <main className={styles.content}>{children}</main>
    </div>
  );
});
MainLayout.propTypes = {
  children: PropTypes.node,
};

export default MainLayout;
