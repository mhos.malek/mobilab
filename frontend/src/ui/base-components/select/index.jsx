import React, { useState, useEffect } from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import Icon from '@/ui/base-components/icon';
import * as styles from './style.scss';
import WithOutsideClickHandler from '../outsideClick';

// I created this component so it's easer to manage ui of select box
export const Select = props => {
  const {
    value,
    showIcon,
    disabled,
    onClick,
    onChange,
    options,
    placeholder,
    name,
  } = props;


  const [selectedOption, setSelectedOption] = useState(value || null);
  const [showList, setShowList] = useState(false);
  const toggleOptionsList = () => {
    setShowList(prevState => !prevState);
  };

  const handleClick = e => {
    if (!disabled) {
      onClick && onClick(e);
      toggleOptionsList();
    }
  };

  const handleItemSelect = selectedOption => {
    setSelectedOption(selectedOption);
    toggleOptionsList();
    onChange && onChange(name, selectedOption);
  };

  useEffect(() => {
    if (value) setSelectedOption(value);
  }, [value]);

  return (
    <WithOutsideClickHandler onOutsideClick={() => setShowList(false)}>
      <div className={styles.container}>
        <div
          className={classnames(styles.textContainer, {
            [styles.disabled]: disabled,
          })}
          onClick={handleClick}
        >
          <div className={styles.text}>
            {selectedOption ? selectedOption.label : placeholder}
          </div>
          {showIcon && (
            <Icon
              name='chevron-up'
              rotate={showList ? 0 : 180}
              className={styles.icon}
            />
          )}
        </div>
        {showList && (
          <div className={styles.boxContainer}>
            {options &&
              options.map((option, index) => {
                const { value, label } = option;
                return (
                  <div
                    key={index}
                    className={styles.optionItem}
                    onClick={e => {
                      // to handle stop close side bar at outside click callback
                      e.stopPropagation();
                      handleItemSelect({ value, label });
                    }}
                  >
                    <div>{label}</div>
                  </div>
                );
              })}
          </div>
        )}
      </div>
    </WithOutsideClickHandler>
  );
};

Select.propTypes = {
  text: PropTypes.string,
  isItemSet: PropTypes.bool,
  showIcon: PropTypes.bool,
  disabled: PropTypes.bool,
  onDismiss: PropTypes.func,
  onClick: PropTypes.func,
  options: PropTypes.array,
  value: PropTypes.shape({
    label: PropTypes.string,
    value: PropTypes.string,
  }),
  onChange: PropTypes.func,
  placeholder: PropTypes.string,
  name: PropTypes.string,
};
Select.defaultProps = {
  disabled: false,
  showIcon: true,
  placeholder: 'Please choose one',
  onChange: () => null,
};
export default Select;
