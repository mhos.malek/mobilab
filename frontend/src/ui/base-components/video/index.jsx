// dependencies
import React from 'react';
import PropTypes from 'prop-types';

// styles
import * as styles from './style.scss';
// I created this component to make handle video with other libraries easier
const VideoPlayer = ({ src, ...options }) => {
  return (
    <video {...options} className={styles.videoPlayerWrapper}>
      <source src={src} type='video/mp4' />
      Your browser does not support the video tag.
    </video>
  );
};

VideoPlayer.propTypes = {
  src: PropTypes.string,
};
export default VideoPlayer;
