import React from 'react';
import PropTypes from 'prop-types';
import * as styles from './style.scss';
import WithOutsideClickHandler from '../outsideClick';

const SwitchToggle = ({
  onChange,
  onClick,
  name,
  checked,
  disabled,
  label,
}) => {
  return (
    <label className={styles.wrapper} onClick={e => onClick && onClick(e)}>
      <div className={styles.switch}>
        <input
          type='checkbox'
          name={name}
          checked={checked}
          onChange={e =>
            onChange && !disabled && onChange(name, e.target.checked)
          }
        />
        <span />
      </div>
      <div className={styles.label}>{label}</div>
    </label>
  );
};

SwitchToggle.propTypes = {
  onClick: PropTypes.func,
  onChange: PropTypes.func,
  name: PropTypes.string,
  checked: PropTypes.bool,
  label: PropTypes.string,
  disabled: PropTypes.bool,
};
export default SwitchToggle;
