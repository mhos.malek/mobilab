// dependencies
import React from 'react';

// ui component
import Card from 'react-bootstrap/Card';
// styles
import * as styles from './style.scss';
// assets
import placeholderImage from '@/assets/images/cropped-fav-icon-512-192x192.png';

const LazyLoadPlaceholder = () => {
  return (
    <Card className={styles.cardWrapper}>
      <div className={styles.cardImageContainer}>
        <img loading='lazy' src={placeholderImage} className={styles.cardImage}/>
      </div>
      <Card.Body>
        {/* <Loading /> */}
      </Card.Body>
    </Card>
  );
};

export default LazyLoadPlaceholder;
