import React, { useRef, useEffect } from 'react';
import PropTypes from 'prop-types';

const WithOutsideClickHandler = ({ onOutsideClick, children }) => {
  const element = useRef();
  const outsideClickHandler = event => {
    const isClickInside = element.current.contains(event.target);
    if (isClickInside) {
      return null;
    }
    return onOutsideClick && onOutsideClick(event);
  };

  useEffect(() => {
    window.addEventListener('click', outsideClickHandler);
    return function() {
      window.removeEventListener('click', outsideClickHandler);
    };
  }, []);

  return <div ref={element}>{children}</div>;
};

WithOutsideClickHandler.propTypes = {
  children: PropTypes.node,
  onOutsideClick: PropTypes.func,
};

export default WithOutsideClickHandler;
