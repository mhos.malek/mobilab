// dependecies
import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import classnames from 'classnames';
// ui component
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';

// logical component
import MultipleItems from '../gallery/multiple-items';

// base components
import SingleItem from '../gallery/single-item';
import Seperator from '../../base-components/seperator';
import Icon from '../../base-components/icon';

// redux
import { hideModalOfImageDetailsAction } from '../../../store/ui/actions';

// styles
import * as styles from './style.scss';

const SelectedItemModal = () => {
  const dispatch = useDispatch();

  const handleClose = () => dispatch(hideModalOfImageDetailsAction());
  const { showModal } = useSelector(state => state.Ui);
  const { selectedImageDetails } = useSelector(state => state.Ui);
  const [currentCarouselIndex, setCurrentCarouselIndex] = useState(0);
  return (
    selectedImageDetails && (
      <Modal show={showModal} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>{selectedImageDetails.title}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {selectedImageDetails.is_album ? (
            <MultipleItems
              imagesList={selectedImageDetails.images}
              onCarouselChange={index => setCurrentCarouselIndex(index)}
            />
          ) : (
            <SingleItem
              src={selectedImageDetails.link}
              type={selectedImageDetails.type}
              title={selectedImageDetails.title}
            />
          )}

          <>
            <h5 className='mt-3 mb-3'> Description </h5>
            <Seperator />

            <p className={classnames('mt-3', styles.text)}>
              {selectedImageDetails.description ||
                (selectedImageDetails.images &&
                  selectedImageDetails.images[currentCarouselIndex] &&
                  selectedImageDetails.images[currentCarouselIndex]
                    .description) ||
                'unfortunatly, no description provided'}
            </p>
          </>

          <h5> Other details </h5>
          <div className='my-3'>
            <Seperator />
          </div>

          <div className={styles.detailsWrapper}>
            <div className={styles.detailItem}>
              <Icon name='heart' color='red' />
              <p className={styles.detailItemTitle}>Up votes</p>
              <p className={styles.detailItemContent}>
                {selectedImageDetails && selectedImageDetails.ups}
              </p>
            </div>
            <div className={styles.detailItem}>
              <Icon name='sad' color='green' />
              <p className={styles.detailItemTitle}>Down votes</p>
              <p className={styles.detailItemContent}>
                {selectedImageDetails && selectedImageDetails.downs}
              </p>
            </div>
            <div className={styles.detailItem}>
              <Icon name='trophy' color='yello' />
              <p className={styles.detailItemTitle}>Score</p>
              <p className={styles.detailItemContent}>
                {selectedImageDetails && selectedImageDetails.score}
              </p>
            </div>
            <div className={styles.detailItem}>
              <Icon name='linegraph' color='gray' />
              <p className={styles.detailItemTitle}>Views</p>
              <p className={styles.detailItemContent}>
                {selectedImageDetails && selectedImageDetails.views}
              </p>
            </div>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant='primary' className="d-flex align-items-center" onClick={handleClose}>
            <span className='mr-1'>Back To gallery</span>
            <Icon name='chevron-up' size='0.8' rotate={90} />
          </Button>
        </Modal.Footer>
      </Modal>
    )
  );
};

export default SelectedItemModal;
