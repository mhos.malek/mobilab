// dependencies
import React, { useContext } from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';
// ui components
import Form from 'react-bootstrap/Form';
// base components
import Select from '../../base-components/select';
import filterContext from '../../../providers/filter-provider/filter-context';
import { filterOptions } from '../../../providers/filter-provider';
import SwitchToggle from '../../base-components/switch-toggle';

const FilterImages = ({ inlineForm }) => {
  const filterProvider = useContext(filterContext);
  const handleOnChange = (name, selectedFilter) => {
    filterProvider.changeFilter(name, selectedFilter);
  };

  return (
    <Form className={classnames({ 'd-flex mx-auto': inlineForm })}>
      <Form.Group
        controlId='section'
        className={classnames({ 'mx-3': inlineForm })}
      >
        <Form.Label>Section</Form.Label>
        <Select
          onChange={handleOnChange}
          name='section'
          options={filterOptions.section}
          value={filterProvider.currentFilters.section}
        />
      </Form.Group>

      <Form.Group
        controlId='sort'
        className={classnames({ 'mx-3': inlineForm })}
      >
        <Form.Label>Sort</Form.Label>
        <Select
          onChange={handleOnChange}
          name='sort'
          options={filterOptions.sort}
          value={filterProvider.currentFilters.sort}
        />
      </Form.Group>

      <Form.Group
        controlId='window'
        className={classnames({ 'mx-3': inlineForm })}
      >
        <Form.Label>Window</Form.Label>
        <Select
          onChange={handleOnChange}
          name='window'
          options={filterOptions.window}
          value={filterProvider.currentFilters.window}
        />
      </Form.Group>
      <Form.Group
        controlId='showViral'
        className={classnames({ 'mx-3 mt-4 d-flex align-items-center': inlineForm })}
      >
        <SwitchToggle
          name='showViral'
          onChange={handleOnChange}
          onClick={e => e.stopPropagation()}
          checked={filterProvider.currentFilters.showViral}
          label='Show Viral'
        />
      </Form.Group>
    </Form>
  );
};

FilterImages.propTypes = {
  inlineForm: PropTypes.bool,
};
export default FilterImages;
