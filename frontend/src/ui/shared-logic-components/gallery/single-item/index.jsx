// dependencies
import React from 'react';
import PropTypes from 'prop-types';

// ui component
import Card from 'react-bootstrap/Card';

import VideoPlayer from '../../../base-components/video';
import { IMAGE_TYPE } from '../fileTypes';
const SingleItem = ({ src, type, title }) => {
  //   const [image] = images;

  const renderVideo = () => {
    return <VideoPlayer src={src} controls />;
  };

  if (IMAGE_TYPE.includes(type)) {
    return <Card.Img variant='top' loading='lazy' src={src} alt={title} />;
  }
  return renderVideo();
};

SingleItem.propTypes = {
  src: PropTypes.string,
  type: PropTypes.string,
  title: PropTypes.string,
};

export default SingleItem;
