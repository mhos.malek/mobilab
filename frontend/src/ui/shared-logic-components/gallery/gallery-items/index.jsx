// dependencies
import React from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';

// ui component
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';

// base componets
import LazyLoad from '@/ui/base-components/lazy-load';
import LazyLoadPlaceholder from '../../../base-components/lazy-load/lazy-load-placeholder';

// redux action
import {
  showModalOfImageDetailsAction,
  setModalDetailsFromImageAction,
} from '../../../../store/ui/actions';

// styles
import * as styles from '../style.scss';
import MultipleItems from '../multiple-items';
import SingleItem from '../single-item';

const GalleryItem = ({
  description,
  title,
  ups,
  downs,
  score,
  is_album,
  is_ad,
  views,
  comment_count,
  type,
  link,
  images_count,
  images,
}) => {
  const dispatch = useDispatch();
  const handleShow = () => {
    dispatch(showModalOfImageDetailsAction());
    dispatch(
      setModalDetailsFromImageAction({
        ups,
        downs,
        score,
        views,
        comment_count,
        description,
        title,
        images,
        link,
        is_album,
      }),
    );
  };

  if (is_ad) return null;
  return (
    <LazyLoad placeholder={<LazyLoadPlaceholder />}>
      <Card className={styles.cardWrapper}>
        <figure className={styles.cardImageContainer}>
          {is_album ? (
            <MultipleItems imagesList={images} />
          ) : (
            <SingleItem src={link} type={type} title={title} />
          )}
        </figure>
        <Card.Body>
          {title && (
            <Card.Title>
              <span>
                <b>{title}</b>
              </span>
              {is_album && images_count > 1 && (
                <p className={styles.imagesCount}>
                  <small> {images_count} images</small>
                </p>
              )}
            </Card.Title>
          )}
          <Card.Text className={styles.truncate}>
            {description
              ? description
              : 'unforunatly, no description provided '}
          </Card.Text>
          <Button onClick={() => handleShow()}>See More</Button>
        </Card.Body>
      </Card>
    </LazyLoad>
  );
};

GalleryItem.propTypes = {
  images_count: PropTypes.number,
  is_album: PropTypes.bool,
  is_ad: PropTypes.bool,
  description: PropTypes.string,
  title: PropTypes.string,
  ups: PropTypes.number,
  downs: PropTypes.number,
  score: PropTypes.number,
  views: PropTypes.number,
  comment_count: PropTypes.number,
  type: PropTypes.string,
  images: PropTypes.array,
  link: PropTypes.string,
};
export default GalleryItem;
