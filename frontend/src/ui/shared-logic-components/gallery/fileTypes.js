const IMAGE_TYPE = ['image/gif', 'image/jpeg', 'image/png'];
const VIDEO_TYPE = 'video/mp4';

export { IMAGE_TYPE, VIDEO_TYPE };
