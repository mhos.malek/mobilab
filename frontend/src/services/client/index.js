const fetcher = url =>
  fetch(url, {
    headers: {
      // Authorization: `Client-ID ${process.env.IMGUR_CLIENT_ID}`,
      // we dont need set headers here, because it has been handled by NodeJs App
    },
  }).then(r => r.json());

export default fetcher;
