import React, { useState } from 'react';
import PropTypes from 'prop-types';
import filterContext from './filter-context';
export const filterOptions = {
  section: [
    { value: 'hot', label: 'HOT' },
    { value: 'top', label: 'TOP' },
    { value: 'user', label: 'USER' },
  ],

  sort: [
    { value: 'top', label: 'Top' },
    { value: 'time', label: 'Time' },
    { value: 'rising', label: 'Rising' },
    { value: 'viral', label: 'Viral' },
  ],
  window: [
    { value: 'day', label: 'Day' },
    { value: 'week', label: 'Week' },
    { value: 'month', label: 'Month' },
    { value: 'year', label: 'Year' },
    { value: 'all', label: 'All' },
  ],
};

const FilterProvider = ({ children }) => {
  const defaultOptions = {
    section: filterOptions.section[0],
    sort: filterOptions.sort[0],
    window: filterOptions.window[0],
    showViral: true,
  };
  const [filters, setFilters] = useState({
    section: defaultOptions.section,
    sort: defaultOptions.sort,
    window: defaultOptions.window,
    showViral: defaultOptions.showViral,
  });

  const changeFilter = (filterName, filterValue) =>
    setFilters(prevFilters => ({ ...prevFilters, [filterName]: filterValue }));

  const valueToPassToContext = {
    currentFilters: filters,
    changeFilter,
  };

  return (
    <filterContext.Provider value={valueToPassToContext}>
      {children}
    </filterContext.Provider>
  );
};

FilterProvider.propTypes = {
  children: PropTypes.node,
};

export default FilterProvider;
