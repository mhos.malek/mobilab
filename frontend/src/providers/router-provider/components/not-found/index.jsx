import React from 'react';
import { Route } from 'react-router-dom';
import PropTypes from 'prop-types';
import { NotFoundPageDetails } from '@/ui/pages';

const NotFoundRoute = ({ index, path, exact }) => {
  return (
    <Route key={index} path={path} exact={exact}>
      <NotFoundPageDetails.component />
    </Route>
  );
};

NotFoundRoute.propTypes = {
  index: PropTypes.number,
  path: PropTypes.string,
  exact: PropTypes.bool,
};
export default NotFoundRoute;
