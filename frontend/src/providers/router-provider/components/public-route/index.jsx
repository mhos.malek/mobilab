import React from 'react';
import PropTypes from 'prop-types';
import MainLayout from '@/ui/layouts';
import { Route } from 'react-router-dom';
const PublicRoute = ({ Component, index, path, exact, meta, hasLayout }) => {
  return hasLayout ? (
    <MainLayout>
      <Route key={index} path={path} exact={exact}>
        {React.cloneElement(<Component />, {
          meta,
        })}
      </Route>
    </MainLayout>
  ) : (
    <Route key={index} path={path} exact={exact}>
      {React.cloneElement(<Component />, {
        meta,
      })}
    </Route>
  );
};

PublicRoute.propTypes = {
  Component: PropTypes.any,
  index: PropTypes.number,
  path: PropTypes.string,
  exact: PropTypes.bool,
  isAuthenticated: PropTypes.bool,
  meta: PropTypes.any,
  hasLayout: PropTypes.bool,
};

export default PublicRoute;
