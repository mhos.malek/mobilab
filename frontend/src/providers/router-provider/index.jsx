import React from 'react';
import { Router, Switch } from 'react-router-dom';
import routes from './routes';
import PrivateRoute from './components/private-route';
import BrowserHistory from './utils/browser-history';
import PublicRoute from './components/public-route';
import NotFoundRoute from './components/not-found';
import pathes from './utils/pathes';

const RouterProvider = () => {
  window.localStorage.setItem('token', 'test token for client ap')
  const isAuthenticated = window.localStorage.getItem('token')
    ? true
    : false;

  return (
    <>
      <Router history={BrowserHistory}>
        <Switch>
          {routes.map((route, index) => {
            return route.isPrivate ? (
              <PrivateRoute
                isAuthenticated={isAuthenticated}
                exact={route.exact || true}
                key={index}
                Component={route.component}
                {...route}
              />
            ) : (
              <PublicRoute
                isAuthenticated={isAuthenticated}
                exact={route.exact || true}
                key={index}
                Component={route.component}
                {...route}
              />
            );
          })}
          <NotFoundRoute
            index={Math.random()}
            path={pathes.NOT_FOUND_PAGE}
            exact={true}
          />
          ;
        </Switch>
      </Router>
    </>
  );
};

export default RouterProvider;
