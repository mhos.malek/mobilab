import React from 'react';
import ThemeContext from './context';
import PropTypes from 'prop-types';
import MobilabTheme from './default-theme';
import './general-style.scss';
// import styles and theme realted stuffs
import 'bootstrap/dist/css/bootstrap.min.css';

// import sass version of theme to use in CSS
import './default-theme/index.scss';

const ThemeProvider = ({ children, theme }) => {
  return (
    <ThemeContext.Provider value={theme}>{children}</ThemeContext.Provider>
  );
};
ThemeProvider.defaultProps = {
  theme: MobilabTheme(),
};

ThemeProvider.propTypes = {
  children: PropTypes.node,
  theme: PropTypes.object,
};

export default ThemeProvider;
