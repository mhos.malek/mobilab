// this is used when you want to cheage theme with js
const MobilabTheme = () => {
  const colors = {
    primary: '#07cac1',
    white: '#ffffff',
    transparent: 'transparent',
    danger: '#d05354',
    disableColor: '#565656',
  };

  const dimensions = {
    full: '100%',
    fullViewPortWidth: '100vw',
    fullViewPortHeight: '100vh',
  };

  return {
    colors,
    dimensions,
  };
};

export default MobilabTheme;
