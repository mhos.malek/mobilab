import { createContext } from 'react';
import MobilabTheme from '../default-theme';

const ThemeContext = createContext(MobilabTheme());

export default ThemeContext;
