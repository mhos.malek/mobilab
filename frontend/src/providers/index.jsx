import React from 'react';
import PropTypes from 'prop-types';
import ThemeProvider from './theme-provider';
import { ToastContainer, Slide } from 'react-toastify';
import { Provider as StoreProvider } from 'react-redux';
import store from '@/store';
import RouterProvider from './router-provider';
import MobilabTheme from './theme-provider/default-theme';
import FilterProvider from './filter-provider';
import ErrorProvider from './error-provider';

const defaultToasOptions = {
  hideProgressBar: false,
  newestOnTop: false,
  closeOnClick: true,
  pauseOnHover: true,
  transition: Slide,
};
const RootProvider = () => {
  return (
    <ErrorProvider>
      <StoreProvider store={store}>
        <ToastContainer {...defaultToasOptions} />
        <ThemeProvider theme={MobilabTheme()}>
          <FilterProvider>
            <RouterProvider />
          </FilterProvider>
        </ThemeProvider>
      </StoreProvider>
    </ErrorProvider>
  );
};

RootProvider.propTypes = {
  children: PropTypes.node,
};
export default RootProvider;
