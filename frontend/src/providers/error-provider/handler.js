import { put } from 'redux-saga/effects';
import NOTIFICATION_MESSAGES from './notification-messages';
import NOTIFICATION_TYPES from './notification-types';
import { showNotificationAction } from '../../store/ui/actions';

class ErrorHandler {
  *networkErrorHandler(error) {
    // do something with errors that come from network
    if (error.response) {
      switch (error.response.status) {
        case 400:
          break;
        default:
          yield put(
            showNotificationAction({
              type: NOTIFICATION_TYPES.ERROR,
              message: NOTIFICATION_MESSAGES.ERRORS.DEFAULT_NETWORK_ERROR,
            }),
          );
      }
    }
    return { error };
  }
  *componentErrorHandler(error) {
    // do Something with component errors that catched in react component. for example send data of error to senrty
    console.log(error);
    yield put(
      showNotificationAction({
        type: NOTIFICATION_TYPES.ERROR,
        message: NOTIFICATION_MESSAGES.ERRORS.DEFAULT_ERROR_BOUNDAY,
      }),
    );
  }
}

export default new ErrorHandler();
