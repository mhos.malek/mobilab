var express = require('express');
var router = express.Router();
var imgUr = require('../controllers/imgUrController');

/* ImgUr Api. */
router.get('/', imgUr.handleIndex);
router.get('/api', imgUr.handleApi);

module.exports = router;
