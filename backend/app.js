// routers
var indexRouter = require('./routes/index');
// urils
var utils = require('./app/utils/methods');
var createApp = require('./app/createApp');

var setConfig = function (expressApp) {
  utils.prepareEnvFiles();
  utils.setViews(expressApp);
  utils.useLogger(expressApp);
  utils.useJson(expressApp);
  utils.useCookies(expressApp);
  utils.setUrlEncode(expressApp);
  utils.setStaticPath(expressApp);
  utils.setCORSHeaders(expressApp, {
    allowAllRequests: true,
  });
};

var configAndStartApp = () => {
  var expressApp = createApp();
  setConfig(expressApp);
  expressApp.use('/', indexRouter);
  return expressApp;
};

var app = configAndStartApp();

module.exports = app;
