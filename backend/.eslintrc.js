module.exports = {
  env: {
    browser: true,
    es6: true,
  },
  extends: ['eslint:recommended', 'prettier'],
  env: {
    node: true,
  },
  plugins: ['prettier'],
};
