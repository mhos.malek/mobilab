const acceptableOptionsToPassToApi = require('../utils/defaultOptions');
/**
 * @param {string} section
 * @param {string} sort
 * @param {string} page
 * @param {string} showViral
 */
module.exports = ImgUrUrlCreator = ({
  section,
  sort,
  page,
  window,
  showViral,
}) => {
  const baseURl = `${process.env.IMGUR_BASE_API_URL}/${process.env.IMGUR_VERSION}`;
  let defaultParams = {
    section: 'hot',
    sort: 'viral',
    page: '0',
    showViral: true,
  };
  if (section === 'top') {
    defaultParams.window = 'day';
  } else {
    defaultParams.window = undefined;
  }

  if (
    (section && !acceptableOptionsToPassToApi.section.includes(section)) ||
    (sort && !acceptableOptionsToPassToApi.sort.includes(sort)) ||
    (window && !acceptableOptionsToPassToApi.window.includes(window))
  ) {
    throw new Error('invalid params requested');
  }

  const shouldIncludeWindowParams = () => {
    // if user selected section wasn't "Top" then we do not pass window options
    // (this is mentiond in API Docs that window parameter need to be set to show window params)
    return window || defaultParams.window
      ? `${window || defaultParams.window}`
      : '';
  };

  return `
  ${baseURl}/gallery/${section || defaultParams.section}/${
    sort || defaultParams.sort
  }/${shouldIncludeWindowParams()}/${page || defaultParams.page}?showViral=${
    showViral || defaultParams.showViral
  }
  `;
};
