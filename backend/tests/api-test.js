//   dependencies for testing
const chai = require('chai');
const chaiHttp = require('chai-http');

// app
const app = require('../app');

// utils needed for test
const urlCreator = require('../utils/urlCreator');

// mock data
const mockGalleryData = require('../mocks/gallery.json');

// configs
chai.use(chaiHttp);

const expect = chai.expect;
chai.should();

describe('Check IMGUR AP', function () {
  // pass a default params to url

  describe('GET default gallery images', function () {
    // should show gallery Images when we call IMGUR API
    it('should get all images in gallery', function (done) {
      const defaultDataLength = mockGalleryData.data.length;
      chai
        .request(app)
        .get('/api')
        .set('Authorization', `Client-ID ${process.env.IMGUR_CLIENT_ID}`)
        .end((err, res) => {
          if (err) done(err);
          this.timeout(10000);
          res.should.have.status(200);
          res.should.have.property('body');
          res.body.should.have
            .property('data')
            .with.lengthOf(Number(defaultDataLength));
          expect(res.body.data.length).to.equal(defaultDataLength);
          done();
        });
    });
  });
});
