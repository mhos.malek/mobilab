## FrontEnd

**Dockerfile**
the main docker file is for dev mode and it will going to use 3002
**Dockerfile.prod**
this file is used to build project for production. it will serve the build result wit nginx and will expose "80" port for the project.

**docker-compose**
this can be used if you want to run the project with docker compose. it will run the prodution file and will expose 3002 port for the client application.

## Backend

**docker-compose**

this can be used if you want to run the project with docker compose. it will run the prodution file and will expose 3001 port for the backend application.

## Run backend and frontend together

if you want to run the backend and frontend project together you can use the docker-compose file in the root of project. this file will build and run both docker files in frontend and backend project. you can access front end project at port 3002 and backend project at port 3001.
