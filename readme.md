
# MobiLab - Code Challenge

  

this project is developed for the Mobilab Solutions gmbh company. this document is some expelantions about the project.

  

**FrontEnd Libraries and frameworks**:

  

1- React

2- Redux

3- Redux-Saga

4- [useSWR](https://swr.now.sh/) as a main http request helper (it can help to cache data and can be used beside default fetch or axios or anything else). 

5- CSS Modules

6 - SASS (there isn't so much Sass and CSS because no need to)

7 - CYPRESS for END2END Test.

8- eslint and prettier for lint and format files

9- Customized Wepack config. (I didn't really need custom webpack config, I just used it because I liked to handle it by my own.)

10- fech polyfill for old browsers that do not support fetch
  

**Backend**:

the backend is just a proxy that handle request from client and call IMGUR API. it also handle things like how much request your account has and when you can call IMGUR api with predifined client Id. I also declared clientID at env file so you can change it to your desired clientID.

  

1- NodeJs Express framework

2- node-fetch for handle http requests.

3- morgan logger

  
  

# Run project

  

if you prefer Docker you can use docker files or docker compose. the more detailed information is in [docker](https://gitlab.com/mhos.malek/mobilab/-/tree/master/doc/docker) docs..

  

otherwise, you can go to projects folder and run both projects and then use the client application at port of 3002.

if you build the frontend project you can build it with npm run build. all the files will be going to save at dist folder.

you can serve it with npm package called "serve"

for install server:

    yarn add global serve

then run local server with:

     serve dist

  

# Project Structure

this is how frontend and backend of this project works. so we start with front end then we go to backend project.

  

# Frontend

  

the main purpose of this project is about moving the main logic from UI to Redux Saga.

```

.

├── analyses # Webpack anaylyzer report too see bundle size in ilustrated view

├── dist # Compiled files (alternative of `build` in others)

├── src # Source files

│ ├── assets # main assets directory

│ │ ├── images # images realted assets

│ ├── hoc # main hoc folder

│ ├── hooks # React hooks that can be reused in project

│ ├── providers # main app providers (inclduing theme provider, router provider that handle routes part and filter provider that handle filter bar in project)

│ ├── store # main store folder that includes modules in app with main store files like reducers and ...

│ │ ├── ui # the UI module (this project has only on module in redux) and file like action types, actions, sagas,reducer and ...)

│ ├── services # services is based on some utils that provide specefic feature in any component or redux saga

│ ├── ui # this folder including everything that we shows in ui(base components, layouts, pages, screens and ...)

│ │ ├── base-components # common and mostly stateless components like switch toggle, lazy load and to show commom ui components

│ │ ├── layouts # layout related component including side filter, top filter, and main layout

│ │ ├── pages # pages (this project doesn't need pages I use them because I wanted to show react router and seo HOC)

│ │ ├── smart-components # components taht connected to an Context or components that have any connection to an state manager.

│ ├── utils # main utils of project. anything that do onething and has no relation to the project itself and can be used anywhere else.

  

├── webpack # webpack config file including (webpack prod, and webpack dev and related utils and plugins)

├── test # Automated tests (Cypress tests)

├── Dockerfile.prod # docker file to build in production wepack config and serve with nginx

├── Dockerfile # docker file to build in dev mode with webpack dev server and hot module replacement

├── docker-compose # docker-compose file to build and run production docker file

├── docs # Documentation files (alternatively `doc`)

│ ├── docker.md # Table of contents

├── LICENSE

└── README.md

└── ... # etc.

  

```

  

## Components:

  

there is a component that includes this types:

- Base-components: components like loading, icon and ... that they are stateless and never connect ro redux or any thing else. they just get props and show the result! only that.

- pages: this components have dumb component and smart component or container component. the dumb component just handle UI and the container pass actions and redux state to dumb component from outside world.

- layout components: main layout related like header, footer and filters ui.

- shared-components: the components that would be shared in pages or layouts and they can be connected to redux or context and they have their own logic in theirself.

  
  
  
  

## Redux Structure

  

every part of the app has it own modules. for this project I just creared the UI module. it's just to show you how is my redux structure.

in here, Saga is responsible to show the notification toasts.

  

## Config folder:

to better organizing, there is an option or config file that store general things about project.

  
  

## Styles:

  

as we talked about that is no need to specefic UI, I haven't done anything special in here. there is CSS Modules with Bootsrap Raect and some custom styles for some components.

  

## providers

  

the providers are main handlers of logics in the application. they provide context for every feature of the app. the most important providers in there are theme provider (to change and use theme in js and main style files), Router provider( I should mention that it doesn't need to be here at all because we have one important page, but it's here just to show my providers to you).

there is alos a main file that put every provider in the project in there(index.jsx)

  

## services

services is some functions that provide specefic feature and usualy use diffrent utils in one service.

  

## FrontEnd TEST - END2END

  

I used cypress as my main test framework and icnludes some tests in here.

  
  
  

# Backend

  

the main purpose of this project is about moving the main logic from UI to Redux Saga.

  

### Components structure:

```

.

├── app # all files that is needed to make express node application server

├── bin # files related to generate and start webserver

├── controllers # controllers of the application routes

├── views # jade files (ui files)

├── routes # router realted files

├── utils # utils that needed in controllers or any other files in app

├── public # main directory for static files. express will server files in this folder.

├── app.js # main handler to start the server

├── Dockerfile # docker file to build and start the server

├── docker-compose # docker-compose file to build and run dockerfile

├── env.production # main env file that include a predifined API account ID for test. you can also use your cliendID in here.

  

├── LICENSE

└── README.md

```

### Backend tests:
I didn't write speceic tests in here, but I have tried to do something and don't send project without any test:) sorry if it's not so good in there.

